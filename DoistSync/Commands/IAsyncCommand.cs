﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace DoistSync.Commands
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}