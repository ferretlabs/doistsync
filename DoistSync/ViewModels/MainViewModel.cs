﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Extensions;
using Cimbalino.Toolkit.Services;
using DoistSync.Commands;
using DoistSync.Models.Api;
using DoistSync.Services;
using Microsoft.Toolkit.Uwp;

namespace DoistSync.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IDoistService _doistService;
        public MainViewModel(
            IDoistService doistService)
        {
            _doistService = doistService;
        }

        public ObservableCollection<ItemViewModel> Tasks { get; set; } = new ObservableCollection<ItemViewModel>();

        public AsyncRelayCommand RefreshCommand => new AsyncRelayCommand(LoadTasks);

        public bool? IsSortedByContent { get; set; } = true;

        public async Task LoadTasks()
        {
            if (!NetworkHelper.Instance.ConnectionInformation.IsInternetAvailable)
            {
                return;
            }

            SetProgressBar("Checking for updated items...");

            try
            {
                var response = await _doistService.GetItemsAsync(CancellationToken.None);
                var items = response.Items;

                AddOrUpdateItems(items);

                await _doistService.SaveItemsToCache(Tasks.Select(x => x.Item));
            }
            catch
            {
            }
            finally
            {
                SetProgressBar();
            }
        }

        private void AddOrUpdateItems(IList<Item> items)
        {
            foreach (var item in items)
            {
                var existingItem = Tasks.FirstOrDefault(x => x.Item.Id == item.Id);
                if (existingItem != null)
                {
                    if (item.IsDeleted)
                    {
                        Tasks.Remove(existingItem);
                    }
                    else
                    {
                        existingItem.UpdateItem(item);
                    }
                }
                else
                {
                    if (!item.IsDeleted)
                    {
                        Tasks.Add(new ItemViewModel(item));
                    }
                }
            }
        }

        public override async Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            if (eventArgs.NavigationMode != NavigationServiceNavigationMode.Back)
            {
                await LoadItemsFromCache();

                await LoadTasks();
            }
        }

        private async Task LoadItemsFromCache()
        {
            var items = await _doistService.LoadItemsFromCache();

            Tasks.Clear();
            Tasks.AddRange(items.Select(x => new ItemViewModel(x)));
        }
    }
}
