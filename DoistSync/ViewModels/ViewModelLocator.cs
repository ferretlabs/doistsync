﻿using Cimbalino.Toolkit.Services;
using DoistSync.Services;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using ScottIsAFool.Windows.MvvmLight.Extensions;

namespace DoistSync.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            //if (ViewModelBase.IsInDesignModeStatic)
            //{

            //}
            //else
            //{
                
            //}

            SimpleIoc.Default.RegisterIf<INavigationService>(() => new NavigationService());
            SimpleIoc.Default.RegisterIf<IStorageService, StorageService>();
            SimpleIoc.Default.RegisterIf<IApplicationSettingsService, ApplicationSettingsService>();
            SimpleIoc.Default.RegisterIf<IDoistService, DoistService>();

            SimpleIoc.Default.Register<MainViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
    }
}
