﻿using System;
using DoistSync.Models.Api;
using Humanizer;

namespace DoistSync.ViewModels
{
    public class ItemViewModel : ViewModelBase
    {
        public Item Item { get; set; }

        public ItemViewModel(Item item)
        {
            UpdateItem(item);
        }

        public string Content => Item?.Content;

        public string CompletionDate => Item?.DueDateUtc?.ToLocalTime().Humanize();

        public DateTimeOffset? SortDate => Item?.DueDateUtc?.ToLocalTime();

        public void UpdateItem(Item item)
        {
            Item = item;
        }
    }
}
