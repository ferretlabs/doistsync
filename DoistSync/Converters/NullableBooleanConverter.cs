﻿using System;
using Windows.UI.Xaml.Data;

namespace DoistSync.Converters
{
    public class NullableBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var b = (bool?) value;
            if (b.HasValue)
            {
                return !b.Value;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            var b = (bool?)value;
            if (b.HasValue)
            {
                return !b.Value;
            }

            return false;
        }
    }
}
