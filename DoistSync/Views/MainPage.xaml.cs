﻿using Windows.UI.Xaml;
using DoistSync.ViewModels;
using Microsoft.Toolkit.Uwp.UI;

namespace DoistSync.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private MainViewModel ViewModel => DataContext as MainViewModel;

        public MainPage()
        {
            InitializeComponent();

            TaskSource.SortDescriptions.Add(new SortDescription("Content", SortDirection.Ascending));
        }

        private void DateSort_OnChecked(object sender, RoutedEventArgs e)
        {
            TaskSource.SortDescriptions.Clear();
            TaskSource.SortDescriptions.Add(new SortDescription("SortDate", SortDirection.Ascending));
        }

        private void ContentSort_OnChecked(object sender, RoutedEventArgs e)
        {
            TaskSource.SortDescriptions.Clear();
            TaskSource.SortDescriptions.Add(new SortDescription("Content", SortDirection.Ascending));
        }
    }
}
