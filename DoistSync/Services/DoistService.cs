﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Cimbalino.Toolkit.Services;
using DoistSync.Models.Api;
using Newtonsoft.Json;

namespace DoistSync.Services
{
    public interface IDoistService
    {
        Task<SyncResponse> GetItemsAsync(CancellationToken cancellationToken);
        Task<List<Item>> LoadItemsFromCache();
        Task SaveItemsToCache(IEnumerable<Item> items);
        void ClearSyncToken();
    }

    public class DoistService : IDoistService
    {
        private const string BaseApiUrl = "https://todoist.com/api/v7/";
        private const string SyncUrl = BaseApiUrl + "sync";
        private const string SyncTokenKey = "SyncToken";
        private const string CacheFile = "TaskCacheFile.json";

        private const string AccessToken = "51c00f594c4dd682945e9d1067c1b181bb059017";

        private readonly HttpClient _httpClient;
        private readonly IApplicationSettingsServiceHandler _localSettings;
        private readonly IStorageServiceHandler _localStorage;

        private string _syncToken;

        public DoistService(
            IApplicationSettingsService applicationSettingsService,
            IStorageService storageService)
        {
            _localStorage = storageService.Local;
            _localSettings = applicationSettingsService.Local;
            _httpClient = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            });

            SetSyncToken(_localSettings.Get<string>(SyncTokenKey));
        }

        public async Task<SyncResponse> GetItemsAsync(CancellationToken cancellationToken)
        {
            var options = $"?sync_token={_syncToken}&token={AccessToken}&resource_types=[\"items\"]";

            var url = SyncUrl + options;

            var response = await _httpClient.GetAsync(new Uri(url), cancellationToken);

            var content = await response.Content.ReadAsStringAsync();

            var settings = new JsonSerializerSettings
            {
                Error = (sender, args) =>
                {
                    var i = 1;
                }
            };

            var item = JsonConvert.DeserializeObject<SyncResponse>(content, settings);

            SetSyncToken(item.SyncToken);

            return item;
        }

        public async Task<List<Item>> LoadItemsFromCache()
        {
            if (await _localStorage.FileExistsAsync(CacheFile))
            {
                var json = await _localStorage.ReadAllTextAsync(CacheFile);
                var items = JsonConvert.DeserializeObject<List<Item>>(json);
                return items;
            }

            return new List<Item>();
        }

        public Task SaveItemsToCache(IEnumerable<Item> items)
        {
            var json = JsonConvert.SerializeObject(items);
            return _localStorage.WriteAllTextAsync(CacheFile, json);
        }

        public void ClearSyncToken()
        {
            _localSettings.Remove(SyncTokenKey);
        }

        private void SetSyncToken(string syncToken)
        {
            _syncToken = string.IsNullOrEmpty(syncToken) ? "*" : syncToken;
            _localSettings.Set(SyncTokenKey, _syncToken);
        }
    }
}
